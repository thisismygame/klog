from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from logger.models import Log

def index(request):
    return HttpResponse("This is the logger index. Yep.")

def detail(request, id):
    return HttpResponse("You're looking at log %s" % id)

def add(request):
    p = get_object_or_404(Log)#, pk=id)
    try:
        new_log = p.get(pk=request.POST['log'])
    except (KeyError, Log.DoesNotExist):
        return render(request, 'logger/detail.html', {
            'log': p,
            'error_message': "You didn't log anything.",
        })
    else:
        new_log.save()
        return HttpResponseRedirect(reverse('log:details', args=(p.id,)))
