from django.contrib import admin
from django.forms import TextInput, Textarea
from django.db import models
from logger.models import Log

class LogAdmin(admin.ModelAdmin):
    fields = ['start_time', 'end_time', 'entry']
    list_display = ('start_time', 'end_time', 'entry')
    list_filter = ['start_time']
    search_fields = ['entry']
    date_hierarchy = 'start_time'

    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size':'100'})},
    }

    def save_model(self, request, obj, form, change):
        obj.logged_by = request.user
        obj.save()

admin.site.register(Log, LogAdmin)
