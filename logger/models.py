from django.db import models
from django.contrib.auth.models import User
from datetime import datetime
from django.utils import timezone

class Log(models.Model):
    entry = models.CharField(max_length=200)
    start_time = models.DateTimeField('start', default=datetime.now)
    end_time = models.DateTimeField('stop', blank=True)
    logged_by = models.ForeignKey(User)
    def __unicode__(self):
        return self.entry

    def logged_today(self):
        return self.start_time >= timezone.now() - datetime.timedelta(days=1)

    def save(self, *args, **kwargs):
        ''' On save, update end_time '''
        if not self.id:
            self.end_time = datetime.today()
        super(Log, self).save(*args, **kwargs)

