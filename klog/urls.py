from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'klog.views.home', name='home'),
    # url(r'^klog/', include('klog.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
   
    url(r'^admin/', include(admin.site.urls)),
    url(r'^logger/', include('logger.urls')),
    url(r'^$', TemplateView.as_view(template_name="logger/index.html"))
)
